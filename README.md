# Backend_Time_Note

## Getting Started

### Environment 

* Nodejs
* Postgres

### Running Dev project

* check src/config/general.json that has application configurations;
* check database.js, change to your database configuration;
* npm install or yarn install;
* npm run dev or yarn run dev;
* the playground is running in the endpoint in src/config/general.json

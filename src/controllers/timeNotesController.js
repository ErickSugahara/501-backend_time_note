const TimeNote = require("../models/TimeNote");
const User = require("../models/User");
const EntryType = require("../models/EntryType");
const Sequelize = require("sequelize");

module.exports = {
  async findByUser(user_id) {
    const timeNotes = await TimeNote.findAll({
      where: {
        user: user_id
      },
      include: {
        association: "timeNotes"
      }
    });

    return timeNotes;
  },

  async takeNote(user) {
    const findUser = await User.findByPk(user);

    if (!findUser) {
      return { error: "user not found", status: 400 };
    }

    const timeNote = await TimeNote.create({
      //Type automatic
      type: 2,
      user: user,
      entry_time: Sequelize.fn("NOW")
    });

    if (!timeNote.dataValues) {
      return { error: "application error", status: 500 };
    } else {
      return timeNote.dataValues.entry_time;
    }
  },

  async store(note) {
    const { entryType, user, id } = note;

    const findId = await TimeNote.findByPk(id);
    const findUser = await User.findByPk(user.id);
    const findEntryType = await EntryType.findByPk(entryType.id);

    if (!findUser) return { error: "user not found", status: 400 };
    if (!findEntryType) return { error: "entryType not found", status: 400 };

    let timeNote = null;
    if (!findId) {
      timeNote = await TimeNote.create({
        ...note,
        type: findEntryType.id,
        user: findUser.id
      });
    } else {
      if (
        await TimeNote.update(
          {
            ...note,
            type: findEntryType.id,
            user: findUser.id
          },
          {
            where: {
              id: id
            }
          }
        )
      ) {
        timeNote = await TimeNote.findByPk(id);
      }
    }

    if (!timeNote.dataValues) {
      return { error: "application error", status: 500 };
    } else {
      return timeNote.dataValues;
    }
  }
};

const Sequelize = require("sequelize");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

const User = require("../models/User");
const TimeNote = require("../models/TimeNote");

const authConfig = require("../config/auth");

module.exports = {
  async index(req, res) {
    const users = await User.findAll({
      include: [
        {
          model: TimeNote,
          as: "timeNotes",
          include: [
            {
              association: "entryType"
            }
          ]
        }
      ]
    });

    return users;
  },

  async find(id) {
    const user = await User.findByPk(id, {
      include: { association: "timeNotes" }
    });

    if (!user) {
      return { status: 400, error: "user not found" };
    }

    return user;
  },

  async store(user) {
    const newUser = await User.create(user);

    return newUser;
  },

  async authenticate(email, password) {
    const user = await User.findOne({
      where: {
        email: email
      }
    });

    if (!user) return { status: 400, error: "Usuário não encontrado" };

    const authenticated = await bcrypt.compare(password, user.get().password);

    if (authenticated) {
      const token = await jwt.sign({ id: user.id }, authConfig.secret, {
        expiresIn: 86400
      });
      return token;
    } else {
      return { status: 403, error: "Usuário não autorizado" };
    }
  },

  async authorize(token) {
    const userId = await jwt.verify(token, authConfig.secret);

    if (!userId) {
      return { status: 403, error: "Token inválido" };
    }

    return { id: userId.id };
  }
};

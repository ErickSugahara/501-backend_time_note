/**
 * Created by Érick Kenji Sugahara
 * Description: Define configuration for sequelize
 */
const Sequelize = require("sequelize");
const bcrypt = require("bcryptjs");

const dbConfig = require("../config/database");

const User = require("../models/User");
const EntryType = require("../models/EntryType");
const TimeNote = require("../models/TimeNote");

const connection = new Sequelize(dbConfig);

//Inicialização de models
User.init(connection);
EntryType.init(connection);
TimeNote.init(connection);

//Associações de models
User.associate(connection.models);
EntryType.associate(connection.models);
TimeNote.associate(connection.models);

//Transforma password em hash
User.beforeCreate(async (user, options) => {
  user.password = await bcrypt.hash(user.password, 8);
});

module.exports = connection;

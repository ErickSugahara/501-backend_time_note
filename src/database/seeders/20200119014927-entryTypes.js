"use strict";

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      "entry_types",
      [
        {
          id: 1,
          name: "manual",
          created_at: Sequelize.fn("NOW"),
          updated_at: Sequelize.fn("NOW")
        },
        {
          id: 2,
          name: "automatic",
          created_at: Sequelize.fn("NOW"),
          updated_at: Sequelize.fn("NOW")
        }
      ],
      {}
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete(
      "entry_types",
      {
        name: {
          [Sequelize.Op.in]: ["manual", "automatic"]
        }
      },
      {}
    );
  }
};

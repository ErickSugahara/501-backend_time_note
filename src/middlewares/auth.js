const jwt = require("jsonwebtoken");
const authConfig = require("../config/auth.json");
const general = require("../config/general");

module.exports = (req, res, next) => {
  const authHeader = req.headers.authorization;
  const path = req.url;

  if (path !== "/" || path === general.playground) return next();

  if (/authenticate/g.test(req.body.query)) return next();

  if (!authHeader) return res.status(401).send({ error: "No token provider" });

  const parts = authHeader.split(" ");

  if (!parts.lentth === 2)
    return res.status(401).send({ error: "Invalid token" });

  const [scheme, token] = parts;

  if (!/^Bearer$/i.test(scheme))
    return res.status(401).send({ error: "Malformatted token" });

  jwt.verify(token, authConfig.secret, (err, decoded) => {
    if (err) return res.status(401).send({ error: "Invalid token" });

    req.userId = decoded.id;
    return next();
  });
};

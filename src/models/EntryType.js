const { Model, DataTypes } = require("sequelize");

class EntryType extends Model {
  static init(sequelize) {
    super.init(
      {
        name: DataTypes.STRING
      },
      {
        sequelize //Connection conf
      }
    );
  }
  static associate(models) {
    this.hasMany(models.TimeNote, {
      foreignKey: "type",
      as: "entryType"
    });
  }
}

module.exports = EntryType;

const { Model, DataTypes } = require("sequelize");
class TimeNote extends Model {
  static init(sequelize) {
    super.init(
      {
        entry_time: DataTypes.DATE
      },
      {
        sequelize //Connection conf
      }
    );
  }

  static associate(models) {
    this.belongsTo(models.User, {
      foreignKey: "user",
      as: "timeNotes"
    });

    this.belongsTo(models.EntryType, {
      foreignKey: "type",
      as: "entryType"
    });
  }
}

module.exports = TimeNote;

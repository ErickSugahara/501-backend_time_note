const { Model, DataTypes } = require("sequelize");

class User extends Model {
  static init(sequelize) {
    super.init(
      {
        name: DataTypes.STRING,
        email: DataTypes.STRING,
        password: DataTypes.STRING
      },
      {
        sequelize //Connection conf
      }
    );
  }

  static associate(models) {
    this.hasMany(models.TimeNote, {
      foreignKey: "user",
      as: "timeNotes"
    });
  }
}

module.exports = User;

const userResolver = require("./userResolver");
const timeNoteResolver = require("./timeNoteResolver");

module.exports = {
  Query: {
    ...userResolver.Query,
    ...timeNoteResolver.Query
  },
  Mutation: {
    ...userResolver.Mutation,
    ...timeNoteResolver.Mutation
  }
};

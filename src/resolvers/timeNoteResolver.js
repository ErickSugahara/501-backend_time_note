const timeNotesController = require("../controllers/timeNotesController");

module.exports = {
  Query: {
    timeNotes: (_, { user }) => timeNotesController.findByUser(user)
  },
  Mutation: {
    takeNote: (_, { user }) => timeNotesController.takeNote(user),
    createNote: (_, { note }) => timeNotesController.store(note)
  }
};

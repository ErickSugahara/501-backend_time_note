const userController = require("../controllers/usersController");

module.exports = {
  Query: {
    users: () => {
      return userController.index();
    },
    user: (_, { id }) => userController.find(id)
  },
  Mutation: {
    createUser: (_, user) => userController.store(user),
    authenticate: (_, { email, password }) =>
      userController.authenticate(email, password),
    authorize: (_, { token }) => userController.authorize(token)
  }
};

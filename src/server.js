const { GraphQLServer } = require("graphql-yoga");
const path = require("path");
const resolvers = require("./resolvers");
const authMiddleware = require("./middlewares/auth");

const general = require("./config/general");

const server = new GraphQLServer({
  typeDefs: path.resolve(__dirname, "schema.graphql"),
  resolvers
});

//get database configuration
require("./database");

//Add authenticate middleware
server.use(authMiddleware);

console.log("Server started. Listening PORT 4000");
server.start({ playground: general.playground });
